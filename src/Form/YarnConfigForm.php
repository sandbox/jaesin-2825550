<?php

namespace Drupal\yarn\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class YarnConfigForm.
 *
 * @package Drupal\yarn\Form
 */
class YarnConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'yarn.YarnConfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yarn_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('yarn.YarnConfig');
    $form['yarn_root'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Yarn root'),
      '#description' => $this->t('Where is the Yarn folder (folder contains the yarn.json file)?'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('yarn_root'),
    ];
    $form['modules_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Modules folder'),
      '#description' => $this->t('The location your yarn modules are installed if something other that node_modules was specified with the --modules-folder flag. i.e.  ` yard add foo --modules-folder=vendor --save`'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('modules_folder'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('yarn.YarnConfig')
      ->set('yarn_root', $form_state->getValue('yarn_root'))
      ->set('modules_folder', $form_state->getValue('modules_folder'))
      ->save();
  }

}
