<p align="center">
  <a href="https://yarnpkg.com/">
    <img alt="Yarn" src="https://github.com/yarnpkg/assets/blob/master/yarn-kitten-full.png?raw=true" width="546">
  </a>
</p>

Provides Drupal library entries for modules managed by the Yarn dependency manager. https://yarnpkg.com/
